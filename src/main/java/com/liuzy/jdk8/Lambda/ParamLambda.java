package com.liuzy.jdk8.Lambda;

/**
 * @author liuzy
 * @description TODO
 * @date 2021/4/7 下午10:07
 */
public class ParamLambda {
    public static void main(String[] args) {

        // 匿名内部类方式
        startThread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + "--> 线程启动");
            }
        });

        // lambda表达式
        startThread(() -> System.out.println(Thread.currentThread().getName() + "--> 线程启动"));
    }

    public static void startThread(Runnable runnable) {
        new Thread(runnable).start();
    }
}
