package com.liuzy.jdk8.Lambda;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author liuzy
 * @description TODO
 * @date 2021/4/7 下午10:30
 */
public class ReturnLambda {
    public static void main(String[] args) {
        String[] arr = {"aaa", "ssss", "ddddd"};

        Arrays.sort(arr, myComparator());
        System.out.println(Arrays.toString(arr));
    }

    public static Comparator<String> myComparator() {

        // 匿名内部类方式
//      return new Comparator<String>(){
//          @Override
//          public int compare(String o1, String o2) {
//              return o2.length() - o1.length();
//          }
//      };
        return (o1, o2) -> o2.length() - o1.length();
    }
}
