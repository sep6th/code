package com.liuzy.jdk8.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author liuzy
 * @description TODO
 * @date 2021/4/25 下午10:33
 */
public class StreamDemo {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(7, 6, 9, 3, 8, 2, 1);
        Stream<Integer> stream = list.stream();
        List<Integer> integerList = stream.filter(x -> x > 7).collect(Collectors.toList());

        integerList.forEach(System.out::println);

        List<Person> personList = new ArrayList<Person>();

        personList.add(new Person("Tom", 8900, 23, "male", "New York"));
        personList.add(new Person("Jack", 7000, 34, "male", "Washington"));
        personList.add(new Person("Lily", 7800, 20, "female", "Washington"));
        personList.add(new Person("Anni", 8200, 22, "female", "New York"));
        personList.add(new Person("Owen", 9500, 34, "male", "New York"));
        personList.add(new Person("Alisa", 7900, 45, "female", "New York"));

        List<String> collect = personList.stream()
                .filter(x -> x.getSalary() > 8000)
                .map(Person::getName)
                .collect(Collectors.toList());

        System.out.println("高于8000的员工姓名：" + collect);
        Arrays.asList("adnm", "admmt", "pot", "xbangd", "weoujgsd");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
