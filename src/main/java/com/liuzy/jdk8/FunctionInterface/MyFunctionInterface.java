package com.liuzy.jdk8.FunctionInterface;

/**
 * 函数式接口：有且只有一个抽象方法的接口，称之为函数式接口
 * 可以包含其他的方法（默认、静态、私有）
 * FunctionalInterface注解作用：检测接口是否是一个函数式接口
 */
@FunctionalInterface
public interface MyFunctionInterface {

    // 定义一个抽象方法
    public abstract void method();
}
