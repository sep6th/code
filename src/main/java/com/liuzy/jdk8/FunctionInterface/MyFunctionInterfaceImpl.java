package com.liuzy.jdk8.FunctionInterface;

import com.liuzy.jdk8.FunctionInterface.MyFunctionInterface;

/**
 * @author liuzy
 * @description TODO
 * @date 2021/4/5 下午4:38
 */
public class MyFunctionInterfaceImpl implements MyFunctionInterface {
    @Override
    public void method() {
        System.out.println("实现类方法执行...");
    }
}
