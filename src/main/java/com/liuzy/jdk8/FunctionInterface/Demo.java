package com.liuzy.jdk8.FunctionInterface;

/**
 * @author liuzy
 * @description TODO
 * @date 2021/4/4 下午5:02
 */
public class Demo {
    public static void main(final String[] args) {

        // 传递接口的实现类
        show(new MyFunctionInterfaceImpl());

        // 传递接口的匿名内部类
        show(new MyFunctionInterface() {
            @Override
            public void method() {
                System.out.println("使用匿名类重写接口中的抽象方法");
            }
        });

        // 使用lambda表达式
        show(
                () -> {
                    System.out.println("使用lambda表达式");
                });

        // 简化,方法体只有一行代码，可以省略花括号和分号
        show(() -> System.out.println("使用简化lambda表达式"));
    }

    public static void show(MyFunctionInterface myFunctionInterface) {
        myFunctionInterface.method();
    }
}
